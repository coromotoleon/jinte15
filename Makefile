all:
	latex sldtfm.tex
	latex sldtfm.tex
	dvips -Ppdf -G0 -o sldtfm.ps sldtfm.dvi
	ps2pdf -sPAPERSIZE=a4 -DMaxSubsetPct=100 -dCompatibilityLevel=1.2 -dSubsetFonts=true -dEmbedAllFonts=true sldtfm.ps sldtfm.pdf

clean:
	rm -f *.log *.dvi *.aux *.blg *.ps *.nav *.out *.snm *.toc *.bbl

